# SC-Future API

This API, which is used to predict future sensor data using a
regression model (built from historical sensor measurements and
corresponding weather data) and future weather prediction data
(obtained from OpenWeatherMap API).

## How to configure

The first step is to configure SC-Future. To do so, you have to edit
`app.ini` configuration file.

Regarding the open weather API configuration, create a free
[account](https://home.openweathermap.org/users/sign_up)
in OpenWeatherMap to get an API key for using OpenWeatherMap API.
Place your API key in the `KEY` parameter of the `[WEATHER]` section
of the configuration file.

Regarding the prediction model, once you have analyzed historical
the linear regression
parameters of your sensor, add the following lines to `app.ini`:

```
[SENSOR.N] ;; some number N

SID = sensor_id_of_your_sensor

TEMPERATURE_LR_SLOPE     = slope_of_temperature_model
TEMPERATURE_LR_INTERCEPT = intercept_of_temperature_model

HUMIDITY_LR_SLOPE        = slope_of_humidity_model
HUMIDITY_LR_INTERCEPT    = intercept_of_humidity_model

```

In order to (partially) support sensors that their model has not been
already computated, if the API is to be called for a sensor that
does not appear in the `app.ini` file, the parameters of the section
`[SENSOR.DEFAULT]` will be considered.


## How to run

To build the Docker image for SC-Future, do the following:

```
docker build -t sc_future .
```

To run SC-Future on port 5002 do the following:

```
docker run -p 5002:5002 sc_future
```

If you don't want to use Docker, install Python3 in your system and
follow the steps shown in the Dockerfile.

## How to access

The API can be accessed by

```
http://localhost:5002/
```

As an example, the call

```
http://localhost:5002/SNV0XYZ/future?startTimestamp=1648450800&lat0=37.93&lon0=23.73&lat1=38.42&lon1=21.82
```

will find the shortest route starting from `(lat=37.93, lon=23.73)`
and ending to `(lat=38.42, lon=21.82)`, and will display several
sensor and weather predictions in the route, provided that the route
will start at timestamp `1648450800`, for the sensor with sensor id
`SNV0XYZ`.

If you leave the timestamp and position parameters empty, i.e.,

```
http://localhost:5002/SNV0XYZ/future
```

the starting point will be Athens, the destination will be Ioannina,
and the starting time will be same time next day.

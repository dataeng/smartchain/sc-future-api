from sc import *
import flask, configparser, json
from flask import request, jsonify
import logging

# initialize flask

app = flask.Flask(__name__)
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
app.config["JSON_SORT_KEYS"] = False

logging.root.setLevel(logging.NOTSET)

# initialize future API

config = configparser.ConfigParser()
config.read('app.ini')

osrm_api = config['ROUTER']['API']

openweathermap_api = config['WEATHER']['API']
openweathermap_key = config['WEATHER']['KEY']

# initialize sensor model config

smodel_config = {}
for section in config.sections():
  if section.startswith("SENSOR."):
    ss = { section: config[section] }
    smodel_config = {**smodel_config, **ss}

# routes

@app.route('/', methods=['GET'])
def home():
  return "<h1>SC-Future API</h1><p>This site is a prototype API which predicts sensor data using open weather data.</p>"


@app.route('/<sensor_id>/future', methods=['GET'])
def get_history(sensor_id):
  
  r = Router(osrm_api, "")
  w = Weather(openweathermap_api, openweathermap_key)
  s = Sensor(smodel_config, sensor_id)
  
  if (len(request.args) > 0):
    
    if ('startTimestamp' in request.args):
      start_time = int(request.args['startTimestamp'])
  
    if ('lon0' in request.args):
      lon0 = request.args['lon0']
    
    if ('lat0' in request.args):
      lat0 = request.args['lat0']
    
    if ('lon1' in request.args):
      lon1 = request.args['lon1']
    
    if ('lat1' in request.args):
      lat1 = request.args['lat1']
    
    start_pos = [ float(lon0), float(lat0) ]
    end_pos   = [ float(lon1), float(lat1) ]
    
    r.setFromToTime(start_pos, end_pos, start_time)
  
  data = r.estimatePositions()
  data = w.extendWithWeather(data)
  data = s.predictSensorData(data)
  
  return jsonify(data)

if __name__ == "__main__":
  app.run()


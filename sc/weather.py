import logging
import requests
import time

class Weather:
  """
  Get weather data using OpenWeatherMap API.
    
  :param api: API url.
  :type api: String
  
  :param key: API key.
  :type key: String
  """
  
  def __init__(self, api, key):
    self.api = api
    self.key = key
  
  
  def getFutureWeather(self, lat, lon):
    """
    Performs an API call to the OpenWeatherMap API for weather
    forecast data for a specific place.
    
    :param lat: Geographical coordinate (latitude)
    :type lat: Float
    
    :param lon: Geographical coordinate (latitude)
    :type lon: Float
    """
    
    URL = self.api
    PARAMS = {
      'lat': lat,
      'lon': lon,
      'units': "metric",
      'appid': self.key
    }
    r = requests.get(url=URL, params=PARAMS)
    
    return r.json()
  
  
  def getWeather(self, lat, lon, timestamp):
    """
    Returns weather prefiction for a specific place and time using
    OpenWeatherMap API. For free users of the API, weather data are
    available for 2 days in the future.
    
    :param lat: Geographical coordinate (latitude)
    :type lat: Float
    
    :param lon: Geographical coordinate (latitude)
    :type lon: Float
    
    :param timestamp: Date (Unix time, UTC time zone)
    :type timestamp: Int
    """
    
    weather = self.getFutureWeather(lat, lon)
    
    now = int(time.time())
          
    diff_in_hours = (timestamp - now) // 3600
    diff_in_days = (timestamp - now) // 86400
    
    if (diff_in_hours < 48 and 'hourly' in weather):
      return weather['hourly'][diff_in_hours]
    if (diff_in_days < 7 and 'daily' in weather):
      return weather['daily'][diff_in_days]
    
    logging.warning(weather)
    return None
  
  
  def extendWithWeather(self, messages):
    """
    Given a set of messages (i.e., dictionaries which contain 
    longitude, latitude and timestamp fields), extend them with
    weather data in a bind-join fashion.
    
    :param lat: messages
    :type lat: List[Dict]
    """
    
    logging.info("extending sensor data with open weather data")
    results = []
    
    for m in messages:
      lon = m['longitude']
      lat = m['latitude' ]
      t   = m['timestamp']
      
      weather = self.getWeather(lat, lon, t)
      
      if (weather != None):
        w = {
          'outside_temperature': weather['temp'],
          'outside_feels_like':  weather['feels_like'],
          'outside_humidity':    weather['humidity'],
          'outside_pressure':    weather['pressure'],
          'outside_clouds':      weather['clouds'],
          'outside_visibility':  weather['visibility'],
          'outside_wind_speed':  weather['wind_speed']
        }
        n = {**m, **w}
        results.append(n)
      
      else:
        results.append(m)
    
    return results


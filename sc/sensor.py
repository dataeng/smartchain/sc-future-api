import logging

class Sensor:
  """
  Implement sensor prediction model using linear rergression.
  
  :param smodel_config: Configuration of the prediction model.
  :type smodel_config: List[Dict]
  
  :param sensor_id: Sensor ID.
  :type sensor_id: String
  """
  
  def __init__(self, smodel_config, sensor_id):
    self.sensor_id = sensor_id
    
    conf = smodel_config.get('SENSOR.DEFAULT')
    
    for s in smodel_config:
      if (sensor_id == smodel_config.get(s)['SID']):
        conf = smodel_config.get(s)
    
    self.temp_slope = conf['TEMPERATURE_LR_SLOPE']
    self.temp_inter = conf['TEMPERATURE_LR_INTERCEPT']
    self.humd_slope = conf['HUMIDITY_LR_SLOPE']
    self.humd_inter = conf['HUMIDITY_LR_INTERCEPT']
  
  
  def predictSensorData(self, messages):
    """
    Given a set of messages (i.e., dictionaries which contain 
    outside_temperature and outside_humidity fields), extend them
    with predictions. Use a linear regression model
    
    :param lat: messages
    :type lat: List[Dict]
    """
    
    logging.info("extending data with sensor data predictions")
    results = []
    
    for m in messages:
      out_temp = m['outside_temperature']
      out_humd = m['outside_humidity']
      
      temp = float(self.temp_inter) + float(self.temp_slope) * float(out_temp)
      humd = float(self.humd_inter) + float(self.humd_slope) * float(out_humd)
      
      s = { 'temperature': temp, 'humidity': humd }
      n = {**m, **s}
      
      results.append(n)
    
    return results


import logging
import requests
import time

class Router:
  """
  Get routing data using OSRM API.
    
  :param api: API url.
  :type api: String
  
  :param key: API key.
  :type key: String
  """
  
  def __init__(self, api, key):
    self.api = api
    self.key = key
    
    self.start_pos = [23.94833319998732, 37.93640496889661]
    self.end_pos   = [20.85729345171304, 39.67306544090891]
    self.start_time = int(time.time()) + 86400
  
  
  def setFromToTime(self, start_pos, end_pos, start_time):
    """
    Set start (time and position) and end position of the route of
    your interest.
    
    :param start_pos: Starting position (longtitude, latitude)
    :type start_pos: [Float,Float]
    
    :param end_pos: Ending GPS position (longtitude, latitude)
    :type end_pos: [Float,Float]
    
    :param start_time: Date to start (Unix time, UTC time zone)
    :type start_time: Int
    """
    
    self.start_pos = start_pos
    self.end_pos = end_pos
    self.start_time = start_time
  
  
  def getRouteInternal(self, s, e, overview):
    """
    Performs an API call to OSRM API to get a route path.
    
    :param s: Starting position (longtitude, latitude)
    :type s: [Float,Float]
    
    :param e: Ending GPS position (longtitude, latitude)
    :type e: [Float,Float]
    
    :param overview: "simplified" or "false"
    :type overview: Int
    """
    
    s_pos = str(s[0]) + "," + str(s[1])
    e_pos = str(e[0]) + "," + str(e[1])
    
    URL = self.api + "/" + s_pos + ";" + e_pos
    PARAMS = {
      'geometries': 'geojson',
      'overview': overview
    }    
    r = requests.get(url=URL, params=PARAMS)
    
    return r.json()
  
  
  def getRouteGeometry(self, start_pos, end_pos):
    """
    Get a geometric object of the proposed route path.
    """
    r = self.getRouteInternal(start_pos, end_pos, 'simplified')
    return r['routes'][0]['geometry']
  
  
  def getRouteDuration(self, start_pos, end_pos):
    """
    Get the duration of the proposed route path.
    """
    r = self.getRouteInternal(start_pos, end_pos, 'false')
    return r['routes'][0]['duration']
  
  
  def estimatePositions(self):
    """
    Call OSRM api to find points and timestamps for the estimated
    route pf the start and end of interest
    """
    logging.info("creating estimated positions using route api result")
    results = []
    
    geometry = self.getRouteGeometry(self.start_pos, self.end_pos)
    coords = geometry['coordinates']
    
    prev_c = coords[0]
    prev_t = self.start_time
    
    for c in coords:
      duration = self.getRouteDuration(prev_c, c)
      p = {
        'longitude': c[0],
        'latitude':  c[1],
        'timestamp': int(prev_t + duration)
      }
      
      results.append(p)
      prev_c = c
      prev_t = prev_t + duration
    
    return results
